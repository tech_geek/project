const gulp = require('gulp');
const html = require('gulp-minify-html');
const css = require('gulp-minify-css');
const script = require('gulp-uglify');

const babel = require('gulp-babel');
const babelcore = require('babel-core');
const es2015 = require('babel-preset-es2015');

const imagemin = require('gulp-imagemin');
//2.html压缩
gulp.task('uglifyhtml', () => {
    return gulp.src('src/html/*.html')
        .pipe(html())//执行压缩
        .pipe(gulp.dest('dist/html/'));
});

//3.压缩css
gulp.task('uglifycss', () => {
    return gulp.src('src/style/*.css')
        .pipe(css())//执行压缩
        .pipe(gulp.dest('dist/style/'));
});

//压缩js
gulp.task('uglifyjs', () => {
    return gulp.src('src/script/*.js')
        .pipe(babel({//先将es6转换成es5
            presets: ['es2015']//es2015->es6  es2016->es7...
        }))
        .pipe(script())//再执行压缩
        .pipe(gulp.dest('dist/script/'));
});


//图片压缩
gulp.task('uglifyimg', () => {
    return gulp.src('src/img/*.{png,gif,jpg,ico}')
        .pipe(imagemin())//执行压缩
        .pipe(gulp.dest('dist/img/'));
});