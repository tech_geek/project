!function($){
    let $id=location.search.substring(1).split("=")[1];
    let $spic=$("#spic");
    let $bf=$("#bf");
    let $sf=$("#sf");
    let $bpic=$("#bpic");
    let $warp=$(".wrap")
    // alert($id)
    $.ajax({
        url:"http://localhost/project/php/meituanshow.php",
        data:{
            sid:$id
        },
        dataType:"json"
    }).done(function(data){
        console.log(data)
        $("#spic img").attr("src",data.url);
        $("#spic img").attr("sid",data.sid);
        $(".detail-tittle b").html(data.tittle);
        $(".p-address em").html(data.address);
        $(".order-price em").html(data.price);
        $bpic.attr("src",data.url);


        //渲染小图
        let $arrliurl=data.liurl.split(",");
        let $strhtml="";
        // console.log($arrliurl)
        $.each($arrliurl,function(index,value){
            $strhtml+=`
                <li>
                    <img src="${value}"/>
                </li>
            `
        })
        $(".ul-list ul").html($strhtml);
        //放大镜效果
        $spic.hover(function(){
            $bf.css("visibility","visible");
            $sf.css("visibility","visible");
            $sf.width($spic.width()*$bf.width()/$bpic.width());
            $sf.height($spic.height()*$bf.height()/$bpic.height());

            //比例
            let $blili=$bpic.width()/$bf.width();
            //sf
            $spic.on("mousemove",function(ev){
                var ev=ev || ev.event;
                let $left=ev.pageX-$warp.offset().left-$sf.width()/2;
                let $top=ev.pageY-$warp.offset().top-$sf.height()/2;
                if($left<0){
                    $left=0;
                }else if($left>=$spic.width()-$sf.width()){
                    $left=$spic.width()-$sf.width();
                }
                if($top<0){
                    $top=0;
                }else if($top>=$spic.height()-$sf.height()){
                    $top=$spic.height()-$sf.height();
                }
                $sf.css({
                    left: $left,
                    top: $top
                });

                $bpic.css({
                    left: -$left*$blili,
                    top: -$top*$blili
                })

            });
            
        },function(){
            $bf.css("visibility","hidden");
            $sf.css("visibility","hidden");
        })


        //小图切换
        $(".ul-list ul").on("click","li",function(){
            let $imgurl=$(this).find("img").attr("src");
            $spic.find("img").attr("src",$imgurl);
            $bpic.attr("src",$imgurl);
        })

        //左右箭头事件
        let $num=4;
        //计算一个li的宽度
        let $liwidth=$(".ul-list ul li").eq(0).outerWidth(true);
        $(".ul-listright").on("click",function(){
            if($(".ul-list ul li").size()>$num){
                $num++;
                $(".ul-listleft").css("color","#333");
                if($(".ul-list ul li").size()==$num){
                    $(".ul-listright").css("color","#fff");
                }
                $(".ul-list ul").animate({
                    left:-$liwidth*($num-4),
                })
            }
        })

        $(".ul-listleft").on("click",function(){
            if($num>4){
                $num--;
                $(".ul-listright").css("color","#333");
                if($num==4){
                    $(".ul-listleft").css("color","#fff");
                }
                $(".ul-list ul").animate({
                    left:-$liwidth*($num-4),
                })
            }
        })
        // 购物车按钮
        let $arrnum=[];
        let $arrsid=[];
        //取出cookie 放入数组判断是第一次还是多次
        function cookietoarr(){
            if($.cookie("cookienum") && $.cookie("cookieid")){
                $arrnum=$.cookie("cookienum").split(",");
                $arrsid=$.cookie("cookieid").split(",");
            }else{
                $arrnum=[];
                $arrsid=[];
            }
        }


        $(".detail-order a").on("click",function(){
            //存下sid
            let $sid=$(this).parents(".detail-info").find("#spic img").attr("sid");
            cookietoarr();
            //第一次点还是多次点？
            if($.inArray($sid,$arrsid)!=-1){//多次点击
                let $num=parseInt($arrnum[$.inArray($sid,$arrsid)])+1;
                $arrnum[$.inArray($sid,$arrsid)]=$num;
                $.cookie("cookienum",$arrnum,{expires:10});
            }else{
                $arrsid.push($sid);
                $.cookie("cookieid",$arrsid,{expires:10});
                $arrnum.push(1);
                $.cookie("cookienum",$arrnum,{expires:10});
            }
        })
    })
}(jQuery)

