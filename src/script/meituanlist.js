!function($){
    let $ullist=$(".ul-list");
    let $arrprcie=[];
    let $arrdefault=[];
    let $pre=null;
    let $next=null;
    $.ajax({
        url:"http://localhost/project/php/meituanlist.php",
        dataType:"json"
    }).done(function(data){
        let $strhtml ="";
        $.each(data, function (index, value) {
            $strhtml += `
            <li>
                <a href="meituanshow.html?sid=${value.sid}" target="_blank">
                    <div class="aimg">
                        <div class="count">${value.sid}</div>
                        <div class="aimg-box">
                            <img class="lazy" data-original="${value.url}" alt=""  width="220" height="125">
                        </div>
                    </div>
                    <div class="info">
                        <h4>${value.tittle}</h4>                                   
                        <p class="desc">
                            ${value.address}
                            <span>人均￥<span class="desc-span">${value.price}</span></span>
                        </p>
                    </div>
                </a>
            </li>
            `;
        });
        $ullist.html($strhtml);


        //实现懒加载
        $(function () {
            $("img.lazy").lazyload({ effect: "fadeIn" });
        });
        $arrprcie=[];
        $arrdefault=[];
        $pre=null;
        $next=null;
        $(".ul-list li").each(function(index,elem){
            $arrprcie[index]=$(this);
            $arrdefault[index]=$(this);
        })
    })

    
    $('.page').pagination({
        pageCount: 4,//总的页数
        jump: true,//是否开启跳转到指定的页数，布尔值。
        coping: true,//是否开启首页和尾页，布尔值。
        prevContent: '上一页',
        nextContent: '下一页',
        homePage: '首页',
        endPage: '尾页',
        callback: function (api) {
            // console.log(api.getCurrent());//获取的页码给后端
            $.ajax({
                url:"http://localhost/project/php/meituanlist.php",
                data:{
                    page:api.getCurrent()
                },
                dataType:"json"
            }).done(function(d){
                // console.log(d)
                let $strhtml ="";
                $.each(d,function(index,value){
                    $strhtml += `
                    <li>
                        <a href="meituanshow.html?sid=${value.sid}" target="_blank">
                            <div class="aimg">
                                <div class="count">${value.sid}</div>
                                <div class="aimg-box">
                                    <img src="${value.url}" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4>${value.tittle}</h4>                                   
                                <p class="desc">
                                    ${value.address}
                                    <span>人均￥<span class="desc-span">${value.price}</span></span>
                                </p>
                            </div>
                        </a>
                    </li>
                    `
                })
                $ullist.html($strhtml);
                $arrprcie=[];
                $arrdefault=[];
                $pre=null;
                $next=null;
                $(".ul-list li").each(function(index,elem){
                    $arrprcie[index]=$(this);
                    $arrdefault[index]=$(this);
                })
                // console.log($arrprcie);
            })
        }
    });

    
    //排序
    const $tagsdefault=$(".tags-default");
    const $tagsprice=$(".tags-price");
    $tagsdefault.on("click",function(){
        // alert(1)
        console.log($arrdefault)
        $.each($arrdefault,function(index,value){
            $ullist.append(value)
        })
        return;
    });
    $tagsprice.on("click",function(){
        // alert(2)
        console.log($arrprcie)
        for(let i=0;i<$arrprcie.length-1;i++){
            for(let j=0;j<$arrprcie.length-i-1;j++){
                $pre=parseFloat($arrprcie[j].find(".desc-span").html());
                
                $next=parseFloat($arrprcie[j+1].find(".desc-span").html());
                // console.log($pre,$next);
                
                if($pre<$next){
                    let $temp=$arrprcie[j];
                    $arrprcie[j]=$arrprcie[j+1];
                    $arrprcie[j+1]=$temp;
                }
            }
        }
        // $ullist.empty();不需要清空 因为jquery里的append追加前会寻找有没有li这个元素对象，有的话就不会追加
        
        $.each($arrprcie,function(index,value){
            $ullist.append(value)
        })
    })

}(jQuery)
