!function($){

    //登录注册
    if(localStorage.getItem("userphone")){
        $(".user").hide();
        $(".admin").show();
        $(".admin span").html(localStorage.getItem("userphone")).css("color","#FE8C00");
    }

    $(".admin a").on("click",function(){
        $(".navl .user").show();
        $(".navl .admin").hide();
        localStorage.removeItem("userphone")
    })


    //二级菜单
    let $lis=$(".banleft-nav-content li");
    let $boxs=$(".banleft-nav-box .box");
    let $navbox=$(".banleft-nav-box");
    $lis.hover(function(){
        $navbox.show();
        $boxs.eq($(this).index()).addClass("active").siblings(".banleft-nav-box .box").removeClass("active");
    },function(){
        $navbox.hide();
    })
    $navbox.on("mouseover",function(){
        $(this).show();
    })
    $navbox.on("mouseout",function(){
        $(this).hide();
    })





    //楼梯效果
    //左侧楼梯开始隐藏 滚动页面到某位置显示
    let $louti = $('.loutinav');
    let $loutili = $('.loutinav li'); 
    let $louceng=$(".louceng");

    function scroll(){
        let $top = $(window).scrollTop();//通过jq方法获取滚动条的top值。
        console.log($top)

        //顶部悬浮事件
        let $box=$(".box-header");
        if($top>=200){
            $box.stop(true).animate({//stop(true) 清楚队列
                top:0,
            })
        }else{
            $box.stop(true).animate({
                top:-60,
            })
        }



        $top >= 500 ? $louti.show() : $louti.hide();

        //滑动滚轮，楼梯对应位置显示active效果  在滚动事件下触发
        $louceng.each(function(index,elem){
            let $lctop = $louceng.eq(index).offset().top + $(elem).height() / 2;

            if ($lctop > $top) {
                $loutili.removeClass('active');
                $loutili.eq(index).addClass('active');
                return false;//一旦满足，each结束。重新拖动滚轮，重新继续比较。
            }
        })
    }
    scroll();
    $(window).on('scroll', function () {
        scroll();
        
    })


    //点击楼梯页面滚动到对应楼层
    $loutili.not('.last').on("click",function(){
        $(window).off("scroll");
        $(this).addClass('active').siblings('li').removeClass('active');
        let $top = $louceng.eq($(this).index()).offset().top;
        //滚动条运动到对应楼层位置
        $('html,body').animate({
            scrollTop: $top,
        });
    })



    //回到顶部
    $('.last').on('click', function () {
        $('html,body').animate({
            scrollTop: 0
        },function(){
            $(window).on('scroll', function () {
                scroll();
            })
        });
    });



    


    //轮播
    let $oli=$(".banright_content_left ol li");
    let $uli=$(".banright_content_left ul li");
    let $right=$(".banright_content_left .right");
    let $left=$(".banright_content_left .left");
    let $num=0;
    //点击小圆圈
    $oli.on("click",function(){
        $num=$(this).index();
        // console.log($num)
        switchover();
    })
    //点击左右箭头
    $right.on("click",function(){
        $num++;
        // console.log($num)
        if($num>$oli.length-1){
            $num=0;
        }
        switchover();
    })

    $left.on("click",function(){
        $num--;
        
        if($num<0){
            $num=$oli.length-1;
        }
        // console.log($num)
        switchover();
    })


    //自动轮播
    let $timer=setInterval(()=>{
        $right.click();
    },2000)

    function switchover(){
        $oli.eq($num).addClass("active").siblings("li").removeClass("active");
        $uli.eq($num).css("opacity",1).siblings("li").css("opacity",0);
    }



    $(".banright_content_left").hover(function(){
        $(".banright_content_left a").css({
            display:"block",
            display:"inline-block",
            width:"50px",
            height:"50px",
            lineHeight:"50px",
            background:"rgba(226,224,224,.5)",
            textAlign:"center",
            borderRadius:"50%",
        });
        //鼠标移入 轮播停止
        clearInterval($timer);
    },function(){
        $(".banright_content_left a").css("display","none");
        $timer=setInterval(()=>{
            $right.click();
        },2000)
    })

    













    //渲染
    $.ajax({
        url:"http://localhost/project/php/meituanindex.php",
        dataType:"json",
    }).done(function(data){
        // console.log(data)
        //第一块渲染
        let $strhtml1="";
        $.each(data.data1,function(index,value){
            // console.log(value)
            // $(".stylelistContent .aimg").find("img").attr("src",value.url);
            $strhtml1+=`
            <a href="#" class="list">
                <div class="aimg">
                    <img class="lazy" data-original="${value.url}" alt="" width="369" height="208">
                </div>
                <div class="b">
                    <div class="btittle">${value.tittle}</div>
                    <div class="bdescribe">${value.desc}</div>
                    <div class="price">
                        <p>
                            <span class="price-symbol">￥</span>
                            <span class="price-current">${value.price}</span>
                        </p>
                        <span class="price-old">门市价￥368</span>
                    </div>
                </div>
            </a>
            `
        })
        $(".stylelistContent").html($strhtml1);

        //第二块渲染
        let $strhtml2="";
        $.each(data.data2,function(index,value){
            $strhtml2+=`
            <a href="#" class="list">
                <div class="aimg">
                    <img class="lazy" data-original="${value.url}" alt="" width="213" height="120">
                </div>
                <div class="b">
                    <div class="btittle">${value.tittle}</div>
                    <div class="bdescribe">${value.desc}</div>
                    <div class="none"></div>
                    <div class="price">
                        <p>
                            <span class="price-symbol">￥</span>
                            <span class="price-current">${value.price}</span>
                        </p>
                        <span class="price-old">门市价￥368</span>
                        <span class="sold">已售14</span>
                    </div>
                </div>
            </a>
            `
        })
        $(".reducecontent").html($strhtml2);


        //第三块渲染
        let $strhtml3="";
        $.each(data.data3,function(index,value){
            $strhtml3+=`
            <div class="box">
                <a href="#">
                    <img class="lazy" data-original="${value.url}" alt="" width="214" height="297">
                    <div class="filmbox">
                        <p class="pOne">
                            <span>
                                <i>${value.count}</i>
                                "  人想看"
                            </span>
                        </p>
                        <p class="pTwo">${value.tittle}</p>
                    </div>
                </a> 
            </div> 
            `
        })
        $(".cat-moviecontent").html($strhtml3);

        //第四块渲染
        let $strhtml4="";
        $.each(data.data4,function(index,value){
            $strhtml4+=`
            <div class="homelist-item">
                <a href="">
                    <div class="item-head">
                        <img data-original="${value.url}" alt="" class="headOneimg lazy"  width="370" height="208">
                        <div class="headTwoimg"></div>
                    </div>
                    <div class="item-continer">
                        <p class="tittle">${value.tittle}</p>
                        <p class="sub-title">
                            <span>${value.desc}</span>
                        </p>
                        <p class="price">
                            <span class="price-icon">￥</span>
                            ${value.price}
                        </p>
                    </div>
                </a>
            </div>
            `
        })
        $(".homelist-continer").html($strhtml4);

        //第五块渲染
        let $strhtml5="";
        $.each(data.data5,function(index,value){
            $strhtml5+=`
            <a href="#" class="lovelist-item">
                <div class="lovelist-box">
                    <div class="lovelist-continer">
                        <div class="lovelist-continer-img">
                            <img class="lazy" data-original="${value.url}" alt="" width="214" height="120">
                        </div>
                        <div class="lovelist-continer-info">
                            <div class="title">${value.tittle}</div>
                            <div class="local">
                                <div class="local-info">${value.desc}</div>
                            </div>
                            <div class="info-foot">
                                <div class="price">
                                    <span>
                                        <span class="money">￥</span>
                                        <span class="number">${value.price}</span>
                                        <span class="desc">起</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            `
        })
        $(".lovelist-body").html($strhtml5);


        //实现懒加载
        $(function () {
            $("img.lazy").lazyload({ effect: "fadeIn" });
        });
    })



}(jQuery)