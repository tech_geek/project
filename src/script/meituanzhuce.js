!function($){
    let $phone=$(".content-phone input");
    let $dynmic=$(".dynamic input");
    let $letter=$(".letter input");
    let $pass=$(".pwd input");
    let $prepass=$(".confirm-password input");
    let $btn=$(".form-btn");
    let $flag0=true;
    let $flag1=true;
    let $flag2=true;
    let $flag3=true;
    $phone.on("blur",function(){
        if($(this).val()!=""){
            let $reg=/^(0086|\+86)?\s*1[3-9]\d{9}$/g;
            let $bool=$reg.test($(this).val()); 
            if($bool){
                $.ajax({
                    type:"post",
                    url:"http://localhost/project/php/meituanzhuce.php",
                    data:{
                        phone:$phone.val(),
                    },
                }).done(function(result){
                    if(!result){//不存在
                        $(".content-phone span").html("√").css("color","green");
                        $flag0=true;
                    }else{//存在
                        $(".content-phone span").html("手机号码已被注册过了").css("color","red"); 
                        $flag0=false;
                    }     
                })
            }else{
                $(".content-phone span").html("手机号不能乱输").css("color","red");
                $(this).val("");
                $flag0=false;
            }  
        }else{
            $(".content-phone span").html("手机号不能为空").css("color","red");
            $flag0=false;
        }
        
    })
    //生成短信验证码
    $dynmic.on("click",function(){
        let $num=$rannum();
        $(".dynamic i").html($num).css("color","#333");
    })

    $letter.on("blur",function(){
        if($(this).val()!=""){
            if($(this).val()===$(".dynamic i").html()){
                $(".letter i").html("√").css("color","green");
                $flag3=true;
            }else{
                $(".letter i").html("短信验证码输入有误").css("color","red");
                $flag3=false;
            }
            
        }else{
            $(".letter i").html("短信验证码不能为空").css("color","red");
            $flag3=false;
        }
    })


    $pass.on("blur",function(){
        if($(this).val()!=""){
            if($flag1){
                $(".pwd i").html("√").css("color","green");
            }else{
                $(".pwd i").html("密码不能乱输").css("color","red");
                $flag1=false;
            }
        }else{
            $(".pwd i").html("密码不能为空").css("color","red"); 
            $flag1=false;
        }
    })


    $pass.on("input",function(){
        let $reg1=/\d+/g;
        let $reg2=/[a-zA-Z]+/g;
        let $reg3=/[\W_]+/g;
        let $count=0;
        if($reg1.test($(this).val())){
            $count++;
        }
        if($reg2.test($(this).val())){
            $count++;
        }
        if($reg3.test($(this).val())){
            $count++;
        }
        switch($count){
            case 1:
                $(".first-span").css("color","orange");
                $flag1=false;
                break;
            case 2:
                $(".sec-span").css("color","yellow");
                $flag1=true;
                break;
            case 3:
                $(".last-span").css("color","green");
                $flag1=true;
                break;
        }

    })


    $prepass.on("blur",function(){
        if($(this).val()!=""){
            if($(this).val()===$pass.val()){
                $(".confirm-password i").html("√").css("color","green");
                $flag2=true;
            }else{
                $(".confirm-password i").html("两次密码输入的不一致").css("color","red"); 
                $flag2=false;
            }
        }else{
            $(".confirm-password i").html("密码不能为空").css("color","red");
            $flag2=false;
        }
    })


    $(".content-form form").on("submit",function(){
        if($phone.val()==""){
            $(".content-phone span").html("手机号不能乱输").css("color","red");
            $flag0=false;
        }
        if($letter.val()==""){
            $(".letter i").html("短信验证码不能为空").css("color","red");
            $flag3=false;
        }
        if($pass.val()==""){
            $(".pwd i").html("密码不能为空").css("color","red"); 
            $flag1=false;
        }
        if($prepass.val()==""){
            $(".confirm-password i").html("密码不能为空").css("color","red");
            $flag2=false;
        }

        if(!$flag0 || !$flag1 || !$flag2 || !$flag3){
            return false;
        }
    })


    function $rannum(){
        //生成随机数
        let $arr=[]
        for(let i=48;i<58;i++){
            $arr.push(String.fromCharCode(i))
        }
        for(let i=97;i<123;i++){
            $arr.push(String.fromCharCode(i))
        }
        let $str="";
        for(let i=0;i<6;i++){
            let $ranindex=parseInt(Math.random()*$arr.length);//随机下标
            if($ranindex>10){
                if(Math.random()>0.5){
                    $str += $arr[$ranindex].toLowerCase()
                }else{
                    $str += $arr[$ranindex]
                }
            }else{
                $str += $arr[$ranindex]
            }
        }
        return $str;
    }
    

}(jQuery)