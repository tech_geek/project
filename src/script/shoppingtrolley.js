!function($){
    

    

    function showlist(sid,num){
        $.ajax({
            url:"http://localhost/project/php/shoppingtrolley.php",
            dataType:"json",
        }).done(function(data){
            // console.log(data)
            $.each(data,function(index,value){
                if(sid==value.sid){
                    
                    let strhtml="";
                    strhtml+=`
                    <tr class="tr-item">
                        <td class="tr-itemheader">
                            <input type="checkbox" checked>
                        </td>
                        <td class="desc">
                            <span sid="${value.sid}">${value.tittle}</span>
                        </td>
                        <td class="tr-itemprice">
                            ￥<span>${value.price}</span>
                        </td>
                        <td class="tr-itemnum">
                            <div class="input-num">
                                <span class="btnleft">-</span>
                                <input type="text" value="${num}">
                                <span class="btnright">+</span>
                            </div>
                        </td>
                        <td class="last-child">
                            ￥<span>${(value.price*num).toFixed(2)}</span>
                        </td>
                        <td class="tr-del">删除</td>
                    </tr>
                    `
                    $(".container-item tbody").append(strhtml);
                    calctotalprice();
                }
            })
        })
    }

    //取cookie
    if($.cookie("cookieid") && $.cookie("cookienum")){
        let $cookieid=$.cookie("cookieid").split(",");
        let $cookienum=$.cookie("cookienum").split(",");
        $.each($cookieid,function(index,value){
            showlist($cookieid[index],$cookienum[index])
        })
    }



    //计算总价
    function calctotalprice(){
        let $count=0;
        $(".tr-item").each(function(index,elem){
            if($(elem).find(".tr-itemheader input").prop("checked")){
                $count+=parseFloat($(elem).find(".last-child span").html());
            }
        })
        $(".order-totalprice em").html($count.toFixed(2));
    }


    //全选
    $(".order-checkbox input").on("change",function(){
        $(".tr-item").find(":checkbox").prop("checked",$(this).prop("checked"));
        calctotalprice()
    })

    let $inputs=$(".tr-item").find(":checkbox");
    $(".container-item tbody").on("change",$inputs,function(){
        if($(".tr-item").find(":checkbox").length===$(".tr-item").find("input:checked").size()){
            $(".order-checkbox input").prop("checked",true);
        }else{
            $(".order-checkbox input").prop("checked",false);
        }
        calctotalprice()
    })
    
    
    //数量改变
    $(".container-item tbody").on("click",".btnright",function(){
        let $number=$(this).parents(".tr-item").find(".input-num input").val();
        $number++;
        $(this).parents(".tr-item").find(".input-num input").val($number);
        $(this).parents(".tr-item").find(".last-child span").html(calcmoney($(this)));
        calctotalprice();
        setcookie($(this));
    })

    $(".container-item tbody").on("click",".btnleft",function(){
        let $number=$(this).parents(".tr-item").find(".input-num input").val();
        $number--;
        if($number<1){
            $number=1;
        }
        $(this).parents(".tr-item").find(".input-num input").val($number);
        $(this).parents(".tr-item").find(".last-child span").html(calcmoney($(this)));
        calctotalprice();
        setcookie($(this));
    })


    $(".container-item tbody").on("input",".input-num input",function(){
        let $reg=/^\d+$/g;
        let $value=$(this).val();
        if(!$reg.test($value)){
            $(this).val(1);
        }
        $(this).parents(".tr-item").find(".last-child span").html(calcmoney($(this)));
        calctotalprice();
        setcookie($(this));
    })


    //计算单价 非渲染
    function calcmoney(obj){
        let $num=parseInt(obj.parents(".tr-item").find(".input-num input").val());
        let $price=parseFloat(obj.parents(".tr-item").find(".tr-itemprice span").html());
        return ($num*$price).toFixed(2);
    }


    //存储cookie
    let $arrnum=[];
    let $arrsid=[];
    //取出cookie 放入数组判断是第一次还是多次
    function cookietoarr(){
        if($.cookie("cookienum") && $.cookie("cookieid")){
            $arrnum=$.cookie("cookienum").split(",");
            $arrsid=$.cookie("cookieid").split(",");
        }else{
            $arrnum=[];
            $arrsid=[];
        }
    }

    function setcookie(obj){
        cookietoarr();
        let $sid=obj.parents(".tr-item").find(".desc span").attr("sid");
        $arrnum[$.inArray($sid,$arrsid)]=obj.parents(".tr-item").find(".input-num input").val();
        $.cookie("cookienum",$arrnum,{expires:10});
    }



    //删除
    function delcookie(sid,$arrsid){
        let $index=-1;
        $.each($arrsid,function(index,value){
            if(sid===value){
                $index=index;
                $arrnum.splice($index,1);
                $arrsid.splice($index,1);
            }
        })
        
        $.cookie("cookienum",$arrnum,{expires:10});
        $.cookie("cookieid",$arrsid,{expires:10});
    }

    $(".container-item tbody").on("click",".tr-del",function(){
        // alert(1)
        cookietoarr();
        if(window.confirm("您确定要删除吗?")){
            $(this).parents(".tr-item").remove();
            delcookie($(this).parents(".tr-item").find(".desc span").attr("sid"),$arrsid);
            calctotalprice();
        }
    })

    $(".ordermiddlie").on("click",function(){
        // alert(2)
        cookietoarr();
        if(window.confirm("您确定要删除选中的商品吗?")){
            $(".tr-item").each(function(index,elem){
                if($(this).find(":checkbox").is(":checked")){
                    $(this).remove();
                    delcookie($(this).find(".desc span").attr("sid"),$arrsid);
                }
            })
            calctotalprice();
        }
    })
}(jQuery);